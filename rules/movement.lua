
local Vec = require 'common.vec'

return function (ruleset)

  local r = ruleset.record

  r:new_property('moving', { direction = Vec(-1, 1) })

  function ruleset.define:move(e)
    function self.when()
      return r:is(e, 'unit') and e.spd <= 0
    end
    function self.apply()
      return e.pos
    end
  end

  function ruleset.define:move(e, dt)
    function self.when()
      return r:is(e, 'unit') and e.spd > 0 and not r:is(e, 'target')
    end
    function self.apply()
      local direction = Vec(-1, 1)
      r:set(e, 'moving', { direction = direction })
      local pos = e.pos
      pos = pos + (direction * dt * e.spd)
      r:set(e, 'unit', { pos = pos })
      return pos
    end
  end

  function ruleset.define:move(e, dt)
    function self.when()
      return r:is(e, 'unit') and r:is(e, 'target') and e.spd > 0
    end
    function self.apply()
      local target = e.target
      if not target then return e.pos end
      if e.pos:dist(target.pos) < e.range then return e.pos end
      local direction = -((e.pos - target.pos):normalized())
      local pos = e.pos
      pos = pos + (direction * dt * e.spd)
      r:set(e, 'moving', { direction = direction })
      r:set(e, 'unit', { pos = pos })
      return pos
    end
  end

end

