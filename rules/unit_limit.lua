
return function (ruleset)

  local r = ruleset.record

  function ruleset.define:update_stats(e, stats)
    self.compose = true
    function self.when()
      return r:is(e, 'unit') and e.unit_bonus > 0
    end
    function self.apply(super)
      -- super aqui atualiza o numero atual de unidades e o gild
      super()
      -- depois, atualizamos o numero maximmo
      stats.max_unit_bonus = stats.max_unit_bonus + e.unit_bonus
    end
  end

  function ruleset.define:kill(e, stats)
    self.compose = true
    function self.when()
      return r:is(e, 'unit') and e.unit_bonus > 0
    end
    function self.apply(super)
      -- mata a unidade normalmente
      super()
      -- depois, atualizamos o numero maximmo
      stats.max_unit_bonus = stats.max_unit_bonus - e.unit_bonus
    end
  end

end

