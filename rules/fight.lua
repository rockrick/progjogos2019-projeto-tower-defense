
return function (ruleset)

  local r = ruleset.record

  function ruleset.define:fight(e1, e2, dt)
    function self.when()
      return r:is(e1, 'unit') and r:is(e2, 'unit')
    end
    function self.apply()
      if e1.pos:dist(e2.pos) <= e1.range then
        e2:take_damage(e1.atk * dt)
        return true
      end
    end
  end

end

