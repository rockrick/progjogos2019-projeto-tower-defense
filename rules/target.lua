
return function (ruleset)

  local r = ruleset.record
  -- possible é uma lista de instancias dos aliados
  -- (alvos possiveis para o monstro)
  r:new_property('target', { target = nil, possible = {} })

  function ruleset.define:set_target(e, allies)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      r:set(e, 'target', 'possible', allies )
      e:search_target()
    end
  end

  -- procura o alvo apropriado mais proximo da unidade e guarda ele
  function ruleset.define:search_target(e)
    function self.when()
      return r:is(e, 'unit') and r:is(e, 'target')
    end
    -- retorna a unidade com tipo == target mais proxima do monstro
    -- ou a mais proxima, se target == 'any'
    function self.apply()
      local possible = r:get(e, 'target', 'possible')
      local target_type = e.target_type
      local min_dist = 1/0
      local ret, capital
      for unit in pairs(possible) do
        if target_type == 'any' or unit.type == target_type then
          local dist = e.pos:dist(unit.pos)
          if min_dist > dist then
            min_dist = dist
            ret = unit
          end
        elseif unit.appearance == 'capital' then
          capital = unit
        end
      end
      return r:set(e, 'target', 'target', ret or capital)
    end
  end

  function ruleset.define:get_target(e)
    function self.when()
      return r:is(e, 'unit') and r:is(e, 'target')
    end
    function self.apply()
      local target = r:get(e, 'target', 'target')
      -- procura um novo alvo caso o atual tenha morrido
      if target and target:is_dead() then
        e:search_target()
      end
      return r:get(e, 'target', 'target')
    end
  end

end

