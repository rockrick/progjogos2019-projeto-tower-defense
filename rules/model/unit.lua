
return function (ruleset)

  local r = ruleset.record

  r:new_property('unit', {
    atk = 0, def = 0 , spd = 0, hp = 1, range = 0, cost = 0, gps = 0,
    unit_bonus = 0, pos = {}, appearance = '', type = '', target_type = 'any',
  })

  function ruleset.define:new_unit(specname, pos)
    local spec = require('database.units.' .. specname)
    function self.when()
      return true
    end
    function self.apply()
      local e = ruleset:new_entity()
      r:set(e, 'named', { name = spec.name })
      r:set(e, 'unit', {
        atk = spec.atk, def = spec.def, spd = spec.spd, hp = spec.max_hp,
        range = spec.range, cost = spec.cost, gps = spec.gps, pos = pos,
        unit_bonus = spec.unit_bonus, target_type = spec.target_type,
        appearance = spec.appearance, type = spec.type,
      })
      return e
    end
  end

  function ruleset.define:update_stats(e, stats)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      stats.gild = stats.gild - e.cost
      stats.num_units = stats.num_units + 1
    end
  end

  function ruleset.define:get_atk(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'atk')
    end
  end

  function ruleset.define:get_def(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'def')
    end
  end

  function ruleset.define:get_spd(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'spd')
    end
  end

  function ruleset.define:get_range(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'range')
    end
  end

  function ruleset.define:get_hp(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'hp')
    end
  end

  function ruleset.define:get_max_hp(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'hp')
    end
  end

  function ruleset.define:get_pos(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'pos')
    end
  end

  function ruleset.define:get_cost(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'cost')
    end
  end

  function ruleset.define:get_gps(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'gps')
    end
  end

  function ruleset.define:get_unit_bonus(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'unit_bonus')
    end
  end

  function ruleset.define:get_appearance(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'appearance')
    end
  end

  function ruleset.define:get_type(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'type')
    end
  end

  function ruleset.define:get_target_type(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:get(e, 'unit', 'target_type')
    end
  end

  function ruleset.define:kill(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return r:set(e, 'unit', 'hp', 0)
    end
  end

  function ruleset.define:is_dead(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return e.hp <= 0
    end
  end

  function ruleset.define:get_description(e)
    function self.when()
      return r:is(e, 'unit')
    end
    function self.apply()
      return ("%s (%d/%d unit)"):format(e.name, e.atk, e.hp)
    end
  end

end

