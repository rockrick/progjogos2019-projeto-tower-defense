
local function table_length(table)
  local count = 0
  for _, _ in pairs(table) do
    count = count + 1
  end
  return count
end

return function (ruleset)

  local r = ruleset.record

  r:new_property('wave', {
    all_waves = {}, current = 1, delay = 3, left = 0 , pending = 0,
  })

  function ruleset.define:new_wave(all_waves)
    function self.when()
      return true
    end
    function self.apply()
      local e = ruleset:new_entity()
      local delay = all_waves[1].delay or 3
      r:set(e, 'wave', {
        all_waves = all_waves, current = 1, left = 0,
        delay = delay,
      })
      return e
    end
  end

  function ruleset.define:start(e)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      return r:set(e, 'wave', { left = e:get_delay() })
    end
  end

  function ruleset.define:poll(e)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      local pending = e:get_pending()
      r:set(e, 'wave', { pending = 0 })
      return pending
    end
  end

  function ruleset.define:update(e, dt)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      r:set(e, 'wave', { left = e.left - dt })
      if e.left <= 0 then
        r:set(e, 'wave', { left = e.left + e.delay })
        r:set(e, 'wave', { pending = e.pending + 1 })
      end
    end
  end

  function ruleset.define:get_next_monster(e, monsters)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      -- retorna o nome da unidade a ser spawnada
      local spawns = e:get_spawns()
      for unit, remaining in pairs(spawns) do
        if unit ~= 'delay' and remaining > 0 then
          spawns[unit] = spawns[unit] - 1
          return unit
        end
      end
      -- se nao tiverem mais unidades para spawnar,
      -- tenta ir para a proxima wave
      return e:next_wave(table_length(monsters))
    end
  end

  function ruleset.define:next_wave(e, alive)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      -- se ainda tiverem unidades da wave vivas, não vai para a proxima
      if alive > 0 then return nil end
      local all = e.all_waves
      -- se acabaram as waves, retorna nil
      if e.current == #all then return "END" end
      r:set(e, 'wave', { current = e.current + 1 })
      r:set(e, 'wave', { delay = all[e.current].delay or 3 })
      r:set(e, 'wave', { left = e.delay })
      return e:get_next_monster()
    end
  end

  function ruleset.define:get_delay(e)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      return r:get(e, 'wave', 'delay')
    end
  end

  function ruleset.define:get_left(e)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      return r:get(e, 'wave', 'left')
    end
  end

  function ruleset.define:get_pending(e)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      return r:get(e, 'wave', 'pending')
    end
  end

  function ruleset.define:get_current(e)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      return r:get(e, 'wave', 'current')
    end
  end

  function ruleset.define:get_all_waves(e)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      return r:get(e, 'wave', 'all_waves')
    end
  end

  function ruleset.define:get_spawns(e)
    function self.when()
      return r:is(e, 'wave')
    end
    function self.apply()
      return e.all_waves[e.current]
    end
  end

end

