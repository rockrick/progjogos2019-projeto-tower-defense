
return function (ruleset)

  local r = ruleset.record

  r:new_property('gild_generator', { gps = 0 })

  function ruleset.define:generate_gild(e)
    function self.when()
      return r:is(e, 'unit') and e.gps <= 0
    end
    function self.apply()
      return 0
    end
  end

  function ruleset.define:generate_gild(e, dt, stats)
    function self.when()
      return r:is(e, 'unit') and e.gps > 0
    end
    function self.apply()
      stats.gild = stats.gild + e.gps * dt
    end
  end

end

