
local UNIT_FUNCTIONS = {}

local Vec = require 'common.vec'
local HealthBar = require 'view.health_bar'

function UNIT_FUNCTIONS.create_unit_at(state, specname, pos)
  local unit = state:rules():new_unit(specname, pos)
  local current = state.stats_values.num_units
  local max = state.stats_values.max_units + state.stats_values.max_unit_bonus
  if unit.cost > state.stats_values.gild then
    print('there is no free lunch')
    state.stats:shake_gild()
    return
  elseif current >= max and
     unit.type ~= 'monster' then
    print('cant create more units!')
    state.stats:shake_units()
    return
  end
  local bar = HealthBar(unit)
  state:view('hud'):add(unit, bar)
  state.atlas:add(unit, pos, unit:get_appearance())
  -- insere na lista, dependendo do lado da unidade
  if unit.type == 'monster' then
    unit:set_target(state.allies)
    state.monsters[unit] = true
  else
    unit:update_stats(state.stats_values)
    state.allies[unit] = true
    -- monstros precisam procurar um alvo novamente, caso a unidade colocada
    -- seja um alvo possivel para ele
    for monster in pairs(state.monsters or {}) do
      monster:search_target()
    end
  end
  return unit
end

function UNIT_FUNCTIONS.get_unit_at(state, pos)
  -- checa se ja tem uma unidade naquela posição
  for unit in pairs(state.atlas:get_all()) do
    if unit.pos == pos then
      return unit
    end
  end
end

function UNIT_FUNCTIONS.create_monster(state, specname)
  local rand = math.random
  local x, y
  if rand(2) == 1 then
    x = rand(-5, 7)
    y = -7
  else
    x = 7
    y = rand(-5, 7)
  end
  local pos = state.battlefield:tile_to_screen(x, y)
  pos = pos + Vec(-rand()*30, rand()*30)
  UNIT_FUNCTIONS.create_unit_at(state, specname, pos)
end

function UNIT_FUNCTIONS.kill_unit(state, unit)
  unit:kill(state.stats_values)
  if unit.type == 'monster' then
    state.monsters[unit] = nil
  else
    state.allies[unit] = nil
    state.stats_values.num_units = state.stats_values.num_units - 1
  end
  -- remove a sprite e a barra de vida da tela
  state.atlas:remove(unit)
  state:view('hud'):remove(unit)
  if unit == state.hovered[1] then
    state.hovered[1] = nil
  end
  unit = nil
end

function UNIT_FUNCTIONS.sell_unit(state, unit)
  local value = math.floor(unit.cost * ((unit.hp / unit.max_hp)^1.5)) / 2
  state.stats_values.gild = state.stats_values.gild + value
  UNIT_FUNCTIONS.kill_unit(state, unit)
end

return UNIT_FUNCTIONS