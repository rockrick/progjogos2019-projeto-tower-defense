
local Vec = require 'common.vec'
local Cursor = require 'view.cursor'
local UnitMenu = require 'view.unit_menu'
local RangeCircle = require 'view.range_circle'
local SpriteAtlas = require 'view.sprite_atlas'
local BattleField = require 'view.battlefield'
local Stats = require 'view.stats'
local State = require 'state'

local UnitFunctions = require 'state.unit_functions'

local PlayStageState = require 'common.class' (State)

function PlayStageState:_init(stack)
  self:super(stack)
  self.stage = nil
  self.cursor = nil
  self.atlas = nil
  self.battlefield = nil
  self.allies = nil
  -- hovered e selected são tabelas para termos
  -- a referencia a eles em unit_menu e range_circle
  self.hovered = {}
  self.selected = {'warrior'}
  self.capital = nil
  self.wave = nil
  self.stats = nil
  self.stats_values = {}
  self.monsters = nil
end

function PlayStageState:enter(params)
  self.stage = params.stage
  self.stats_values.gild = params.stage.starting_gild
  self.stats_values.max_units = params.stage.starting_unit_limit
  self.stats_values.max_unit_bonus = 0
  self.stats_values.num_units = 0
  self:_load_view()
  self:_load_units()
end

function PlayStageState:leave()
  self:view('bg'):remove('battlefield')
  self:view('fg'):remove('atlas')
  self:view('bg'):remove('cursor')
  self:view('hud'):remove('stats')
end

function PlayStageState:_load_view()
  self.battlefield = BattleField()
  self.atlas = SpriteAtlas()
  self.cursor = Cursor(self.battlefield)
  self.circle = RangeCircle(self.hovered)
  local _, right, top, _ = self.battlefield.bounds:get()
  self.stats = Stats(Vec(right + 16, top), self.stats_values)
  self.unit_menu = UnitMenu(Vec(right+16, top+72), self.atlas, self.selected)
  self:view('bg'):add('battlefield', self.battlefield)
  self:view('fg'):add('atlas', self.atlas)
  self:view('bg'):add('cursor', self.cursor)
  self:view('bg'):add('range_circle', self.circle)
  self:view('hud'):add('stats', self.stats)
  self:view('hud'):add('units_menu', self.unit_menu)
end

function PlayStageState:_load_units()
  local pos = self.battlefield:tile_to_screen(-6, 6)
  self.allies = {}
  self.capital = UnitFunctions.create_unit_at(self, 'capital', pos)
  self.wave = self:rules():new_wave(self.stage.waves)
  self.wave:start()
  self.monsters = {}
end

function PlayStageState:on_keypressed(key)
  if key == 'escape' then
    self.hovered[1] = nil
  elseif key == '1' then
    if self.unit_menu:get_unit('warrior') then
      self.unit_menu:remove_unit('warrior')
    else
      self.unit_menu:add_unit('warrior')
    end
  elseif key == '2' then
    if self.unit_menu:get_unit('capital') then
      self.unit_menu:remove_unit('capital')
    else
      self.unit_menu:add_unit('capital')
    end
  end
end

function PlayStageState:on_mousepressed(_, _, button)
  if self.cursor:is_in_battlefield() then
    local pos = Vec(self.cursor:get_position())
    local unit = UnitFunctions.get_unit_at(self, pos)
    if button == 1 then
      local circle = self:view('bg'):get('range_circle')
      if unit then
        self.hovered[1] = unit
        return
      end
      self.hovered[1] = nil
      
      -- cria a unidade
      UnitFunctions.create_unit_at(self, self.selected[1], pos)
    elseif button == 2 then
      if unit then
        UnitFunctions.sell_unit(self, unit)
      end
    end
  else -- mouse está fora do battlefield
    if button == 1 then
      local true_pos = self.cursor:get_true_position()
      local hovered = self.unit_menu:get_hovered_unit(true_pos)
      if hovered then
        self.selected[1] = hovered
      end
    end
  end
end

function PlayStageState:update(dt)
  self.wave:update(dt)
  local pending = self.wave:poll()
  while pending > 0 do
    local monster = self.wave:get_next_monster(self.monsters)
    if monster == "END" then
      print('victory!!')
    elseif monster then
      UnitFunctions.create_monster(self, monster)
    end
    pending = pending - 1
  end
  -- movimenta todos os monstros
  for monster in pairs(self.monsters) do
    if not monster:is_dead() then
      local sprite_instance = self.atlas:get(monster)
      sprite_instance.position = monster:move(dt)
      monster:fight(monster.target, dt)
    else -- assassina o monstro de forma brutal
      UnitFunctions.kill_unit(self, monster)
    end
  end

  for ally in pairs(self.allies) do
    if ally:is_dead() then
      UnitFunctions.kill_unit(self, ally)
      break
    end
    -- ataca um monstro
    for monster in pairs(self.monsters) do
      if not monster:is_dead() and ally:fight(monster, dt) then
        break
      end
    end
    -- gera dinheiro
    ally:generate_gild(dt, self.stats_values)
  end
end

return PlayStageState

