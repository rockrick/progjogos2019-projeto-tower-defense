
local Vec = require 'common.vec'
local PALETTE_DB = require 'database.palette'
local HealthBar = require 'common.class' ()

local BAR_W = 28
local BAR_H = 6
local INNER_BAR_W = 26
local INNER_BAR_H = 4

function HealthBar:_init(unit)
  self.position = Vec()
  self.unit = unit
end

function HealthBar:get_progress()
  assert(self.unit.max_hp > 0)
  return self.unit.hp/self.unit.max_hp
end

function HealthBar:update(_)
  self.position = self.unit:get_pos()
end

function HealthBar:draw()
  local progress = self:get_progress()
  -- não desenha a barra se estiver de vida cheia
  if progress >= 1 then return end
  local g = love.graphics
  g.push()
  g.translate((self.position + Vec(0,-25)):get())
  g.setColor(1, 1, 1, 1)
  g.setLineWidth(2)
  g.rectangle('line', -BAR_W / 2, -BAR_H / 2, BAR_W, BAR_H)
  if self.unit.type == 'monster' then
    g.setColor(PALETTE_DB.red)
  else
    g.setColor(PALETTE_DB.green)
  end
  g.rectangle('fill', -INNER_BAR_W / 2, -INNER_BAR_H / 2,
                      INNER_BAR_W*progress , INNER_BAR_H)
  g.pop()
end

return HealthBar