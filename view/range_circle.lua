
local RangeCircle = require 'common.class' ()

function RangeCircle:_init(hovered_unit)
  self.hovered_unit = hovered_unit
end

function RangeCircle:draw()
  local unit = self.hovered_unit[1]
  if not unit then return end
  local g = love.graphics
  g.push()
  g.translate(unit.pos:get())
  g.setLineWidth(1)
  g.setColor(.2, 1, .2, .4)
  g.circle('line', 0, 0, unit.range, unit.range)
  -- 3D effect
  -- for i = 1,15 do
  --   g.translate(0,-1)
  --   g.setLineWidth(1)
  --   g.setColor(.2, 1, .2, .4-(i/15))
  --   g.circle('line', 0, 0, self.radius, self.radius)
  -- end
  g.pop()
end

return RangeCircle