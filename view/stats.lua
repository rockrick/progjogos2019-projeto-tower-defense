
local Tween = require 'common.tween'
local Vec = require 'common.vec'
local PALETTE_DB = require 'database.palette'
local Stats = require 'common.class' ()

function Stats:_init(position, stats)
  self.stats = stats
  self.position = position
  self.gild_offset = Vec(0,0)
  self.units_offset = Vec(0,0)
  self.in_tweens = {}
  self.out_tweens = {}
  self.back_tweens = {}
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
  self.font:setFilter('nearest', 'nearest')
end

function Stats:shake_gild()
  self.in_tweens.gild = Tween.new(0.08, self, {gild_offset = Vec(-10, 0)}, 'inCubic')
  self.out_tweens.gild = Tween.new(0.1, self, {gild_offset = Vec(10, 0)}, 'inCubic')
  self.back_tweens.gild = Tween.new(0.08, self, {gild_offset = Vec(0, 0)}, 'inCubic')
end

function Stats:shake_units()
  self.in_tweens.units = Tween.new(0.08, self, {units_offset = Vec(-10, 0)}, 'inCubic')
  self.out_tweens.units = Tween.new(0.1, self, {units_offset = Vec(10, 0)}, 'inCubic')
  self.back_tweens.units = Tween.new(0.08, self, {units_offset = Vec(0, 0)}, 'inCubic')
end

function Stats:draw()
  local g = love.graphics
  g.push()
  g.setFont(self.font)
  g.setColor(1, 1, 1)
  g.translate(self.position:get())
  g.push()
  g.translate(self.gild_offset:get())
  g.setColor(PALETTE_DB.gold)
  g.print(("Gild: %d"):format(self.stats.gild))
  g.pop()
  g.setColor(1, 1, 1)
  g.translate(self.units_offset.x, self.font:getHeight())
  g.print(("Units: %d/%d"):format(self.stats.num_units,
  self.stats.max_units + self.stats.max_unit_bonus))
  g.pop()
end

function Stats:update(dt)
  for _, in_tween in pairs(self.in_tweens) do
    if in_tween:update(dt) then
      for _, out_tween in pairs(self.out_tweens) do
        if out_tween:update(dt) then
          for _, back_tween in pairs(self.back_tweens) do
            if back_tween:update(dt) then
              self.back_tween = nil
              self.out_tween = nil
              self.in_tween = nil
            end
          end
        end
      end
    end
  end
end

return Stats

