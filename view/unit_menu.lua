
local Vec = require 'common.vec'
local Box = require 'common.box'
local PALETTE_DB = require 'database.palette'
local UnitMenu = require 'common.class' ()

local DISPLAY_ORDER = {
  'capital', 'warrior', 'archer', 'barracks', 'gild_mine',
}
local AVAILABLE = {
  warrior = true, archer = true, gild_mine = true, barracks = true,
}
-- maximo de unidades em uma coluna do menu
local Grid_h = 3

function UnitMenu:_init(position, atlas, selected)
  local h = love.graphics.getHeight() / 1.5
  self.bounds = Box(-4, 196, 0, h)
  self.boxes = {}
  self.unitspecs = {}
  self.atlas = atlas
  self.position = position
  self.selected = selected
  self:_load_unit_sprites()
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
  self.small_font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 18)
  self.font:setFilter('nearest', 'nearest')
  self.small_font:setFilter('nearest', 'nearest')
end

function UnitMenu:_load_unit_sprites()
  local i, j = 1, 0
  local box_pos, box_size = Vec(0,-36), Vec(18,18)
  for _, name in ipairs(DISPLAY_ORDER) do
    if AVAILABLE[name] then
      -- name = name:sub(1, -5)
      local unitspec = require('database.units.' .. name)
      local offset = Vec(j * 48, i * 48) + Vec(16,16)
      self.atlas:add(name, self.position + offset, unitspec.appearance)
      self.boxes[name] = Box.from_vec(box_pos + offset, box_size)
      self.unitspecs[name] = unitspec
      if i == Grid_h then
        i = 1
        j = j + 1
      else
        i = i + 1
      end
    end
  end
end

function UnitMenu:get_unit(unit_name) --luacheck: no self
  return AVAILABLE[unit_name]
end

function UnitMenu:add_unit(unit_name)
  if AVAILABLE[unit_name] then return end
  AVAILABLE[unit_name] = true
  self:_load_unit_sprites()
end

function UnitMenu:remove_unit(unit_name)
  if not AVAILABLE[unit_name] then return end
  AVAILABLE[unit_name] = false
  self.atlas:remove(unit_name)
  self.boxes[unit_name] = nil
  self:_load_unit_sprites()
end

function UnitMenu:get_hovered_unit(pos)
  -- tira os translates, pois a posição do cursor é absoluta, e a das
  -- caixas é relativa
  pos = pos - self.position - Vec(0, self.font:getHeight())
  for unit, box in pairs(self.boxes) do
    if box:is_inside(pos) then
      return unit
    end
  end
  return nil
end

function UnitMenu:draw()
  local g = love.graphics
  g.push()
  g.setFont(self.font)
  g.setColor(1, 1, 1)
  g.translate(self.position:get())
  g.print("Create unit")
  g.translate(0, self.font:getHeight())
  g.setLineWidth(2)
  g.rectangle('line', self.bounds:get_rectangle())
  if self.hovered then
    local box = self.boxes[self.hovered]
    local cost = self.unitspecs[self.hovered].cost or 0
    local pos_x, _, _, pos_y = box:get()
    g.setColor(PALETTE_DB.green)
    g.rectangle('line', box:get_rectangle())
    g.push()
    g.translate(pos_x, pos_y)
    g.setColor(PALETTE_DB.gold)
    g.setFont(self.small_font)
    g.print(cost .. "G")
    g.pop()
  end
  if self.boxes[self.selected[1]] then
    g.setColor(.4, 1, .4, .4)
    g.rectangle('line', self.boxes[self.selected[1]]:get_rectangle())
  end
  g.pop()
end

function UnitMenu:update(_)
  self.mouse_pos = Vec(love.mouse.getPosition())
  local pos = self.mouse_pos
  pos = pos - self.position - Vec(0, self.font:getHeight())
  for unit, box in pairs(self.boxes) do
    if box:is_inside(pos) then
      self.hovered = unit
      return
    end
  end
  self.hovered = nil
end

return UnitMenu

