
local Stack = require 'stack'
local View  = require 'view'

local _game
local _stack

local RULE_MODULES = { 'rules', 'rules.model' }
local RULESETS = {
  'unit', 'wave', 'damage', 'fight', 'movement', 'target', 'gild_generation',
  'unit_limit'
}

function love.load()
  math.randomseed( os.time() )
  local path = love.filesystem.getRequirePath()
  love.filesystem.setRequirePath("lib/?.lua;lib/?/init.lua;" .. path)
  _game = {
    bg_view = View(),
    fg_view = View(),
    hud_view = View(),
    rules = require 'ur-proto' (RULE_MODULES, RULESETS)
  }
  _stack = Stack(_game)
  _stack:push('choose_stage')
end

function love.update(dt)
  _stack:update(dt)
  _game.bg_view:update(dt)
  _game.fg_view:update(dt)
  _game.hud_view:update(dt)
end

function love.draw()
  _game.bg_view:draw()
  _game.fg_view:draw()
  _game.hud_view:draw()
end

for eventname, _ in pairs(love.handlers) do
  love[eventname] = function (...)
    _stack:forward(eventname, ...)
  end
end

