
return {
  texture = 'kenney-1bit.png',
  frame_width = 16,
  frame_height = 16,
  gap_width = 1,
  gap_height = 1,
  sprites = {
    invalid = {
      frame = { 24, 25 },
      color = 'red'
    },
    capital = {
      frame = { 5, 19 },
      color = 'white'
    },
    gild_mine = {
      frame = { 8, 21 },
      color = 'gold'
    },
    barracks = {
      frame = { 4, 20 },
      color = 'beige'
    },

    cursor = {
      frame = { 29, 14 },
      color = 'gray'
    },
    green_slime = {
      frame = { 27, 8 },
      color = 'green'
    },
    blue_slime = {
      frame = { 27, 8 },
      color = 'blue'
    },
    warrior = {
      frame = { 28, 0 },
      color = 'blue'
    },
    archer = {
      frame = { 32, 0 },
      color = 'red'
    },
    priest = {
      frame = { 24, 0 },
      color = 'white'
    },
    business_guy = {
      frame = { 28, 4 },
      color = 'grey'
    },
  }
}

