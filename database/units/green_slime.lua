
return {
  name = "Green Slime",
  appearance = 'green_slime',
  type = 'monster',
  target_type = 'structure',
  max_hp = 4,
  atk = 1,
  def = 1,
  spd = 15,
  range = 30,
}

