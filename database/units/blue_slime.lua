
return {
  name = "Blue Slime",
  appearance = 'blue_slime',
  type = 'monster',
  target_type = 'any',
  max_hp = 15,
  atk = 3,
  def = 6,
  spd = 25,
  range = 30,
}

