
return {
  name = "Gild Mine",
  appearance = 'gild_mine',
  type = 'structure',
  max_hp = 30,
  cost = 50,
  gps = 2, -- gild per second
}

