
return {
  name = "Archer Troop",
  appearance = 'archer',
  type = 'troop',
  max_hp = 6,
  atk = 2,
  def = 0,
  range = 70,
  cost = 20,
}

