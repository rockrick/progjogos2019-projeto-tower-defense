
return {
  name = "Warrior Troop",
  appearance = 'warrior',
  type = 'troop',
  max_hp = 10,
  atk = 1,
  def = 1,
  range = 50,
  cost = 15,
}

