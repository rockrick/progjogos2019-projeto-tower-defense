
return {
  name = "Barracks",
  appearance = 'barracks',
  type = 'structure',
  max_hp = 25,
  cost = 100,
  unit_bonus = 5,
}

