
return {
  title = 'Slime Infestation',
  starting_gild = 2000,
  starting_unit_limit = 10,
  waves = {
    { green_slime = 2 , delay = 2},
    { blue_slime = 1 , delay = 1},
    { green_slime = 30, delay = .1},
    { blue_slime = 5, green_slime = 50, delay = .05},
    { blue_slime = 10, green_slime = 10, delay = .025},
  }
}

